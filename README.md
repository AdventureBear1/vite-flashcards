# React + TypeScript + Vite

This is a Flashcard app built with React, Vite, Typescript, Zustand, Tailwind, react-query and json-server.  
To use it you'll need to run the json-server locally, in addition to running the dev mode.

## Instructions
clone repository
npm install 

In one terminal window: 
npm run dev

In a second terminal window:
json-server watch --data/db.json


## A few Resources that were helpful

https://medium.com/@farhoddev/how-to-create-a-reusable-react-button-component-bac643ca4594
https://dev.to/frontenddeveli/simple-mutations-with-tanstack-query-and-nextjs-4b0m
https://strictlywebdev.com/blog/json-server-put-patch-post-delete/
https://shekhargulati.com/2019/07/10/how-to-setup-json-server-to-use-custom-id-and-route/
https://quickref.me/generate-an-unique-and-increment-id.html
[https://paletton.com/#uid=73g0S0kqUjc9ko8iOm3AXfQSRbN
]()https://strictlywebdev.com/blog/json-server-put-patch-post-delete/
https://saltsthlm.github.io/protips/jsonServer.html
https://medium.com/@joris.l/tutorial-zustand-a-simple-and-powerful-state-management-solution-9ad4d06d5334

https://my-json-server.typicode.com/

http://paletton.com/#uid=53+0o0ko2Rm01SNcmS9JoVJ++tc
https://uicolors.app/create
